# Install Dependencies
FROM node:19-alpine AS deps
WORKDIR /usr/src/app
ENV NODE_ENV production
RUN npm install -g pnpm
COPY package.json pnpm-lock.yaml* ./

RUN pnpm install --frozen-lockfile

# Build
FROM node:19-alpine AS build
WORKDIR /usr/src/app
COPY --from=deps /usr/src/app/node_modules ./node_modules
COPY . .

# npm build or pnpm build doesn't matter. pnpm is just use for package management.
RUN npm run build

# Production image
FROM nginx AS prod
COPY --from=build /usr/src/app/dist /usr/share/nginx/html

EXPOSE 3000

ENV PORT 3000


