import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';
import windiCSSPlugin from 'vite-plugin-windicss';

export default defineConfig({
  plugins: [solidPlugin(), windiCSSPlugin()],
  server: {
    port: 3000,
  },
  build: {
    target: 'esnext',
  },
});
