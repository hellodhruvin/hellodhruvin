# Dhruvin Purohit

Hello there,

I am Dhruvin Purohit.

I am an Open Source Enthusiast, A Self Taught Developer and a Programming Geek.

I always try to learn something new or implement what I know already

I like to work on:
* Backend development
* Software development
* Linux System Administration
* Serverside Applications and APIs
* Code Refactoring
* Documentation
* Reverse Engineering

I like to work using:
* Rust
  * Warp.rs
  * Rocket.rs
* C
* JavaScript
  * TypeScript
  * Prisma ORM
  * Next.js
  * React
  * Deno Runtime
  * Express.js
  * Svelte
  * Solid.js
* Tailwind CSS
* Python
* Java
* Databases
  * Redis
  * MongoDB
  * SQL
  * MySQL
  * PostgreSQL
