import { Component } from "solid-js";
import {
  Table,
  TableCellHorizontalSplit,
  TableCellVerticalSplit,
} from "./components/Tables";
import { transition } from "./utils";

const Plans: Component = () => {
  return (
    <>
      <div class="text-gray-100/60">
        <Table>
          <div class="p-3 text-2xl text-center">Currently I want to learn</div>
          <TableCellVerticalSplit>
            <div
              class={`${transition} flex justify-center items-center w-full text-xl`}
            >
              Databases
            </div>
            <TableCellHorizontalSplit class="w-full">
              <div class={`${transition} p-2`}>Redis</div>
              <div class={`${transition} p-2`}>Neo4J</div>
              <div class={`${transition} p-2`}>SurrealDB</div>
              <div class={`${transition} p-2`}>Apache Cassandra</div>
            </TableCellHorizontalSplit>
          </TableCellVerticalSplit>
          <div
            class={`${transition} flex justify-center items-center w-full text-xl p-2`}
          >
            Websockets
          </div>
          <TableCellVerticalSplit>
            <div
              class={`${transition} flex justify-center items-center w-full text-xl`}
            >
              Android/iOS
            </div>
            <div class={`${transition} p-2 w-full`}>Flutter</div>
          </TableCellVerticalSplit>
        </Table>
      </div>
    </>
  );
};

export default Plans;
