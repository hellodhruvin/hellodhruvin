import { Component } from "solid-js";
import {
  Table,
  TableCellHorizontalSplit,
  TableCellVerticalSplit,
} from "./components/Tables";
import { transition } from "./utils";

const Setup: Component = () => {
  return (
    <div class="space-y-8 text-gray-100/60">
      <Table>
        <div class="p-3 text-xl text-center">Hardware</div>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>Laptop</div>
          <div class={`${transition} p-2 w-full text-right`}>
            Lenovo V15 G2 ALC
          </div>
        </TableCellVerticalSplit>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>CPU</div>
          <div class={`${transition} p-2 w-full text-right`}>
            AMD Ryzen 3 5300U
          </div>
        </TableCellVerticalSplit>
        <TableCellVerticalSplit>
            <div class={`${transition} p-2 w-1/3`}>Memory</div>
          <TableCellHorizontalSplit class="w-full">
            <div class={`${transition} p-2 text-right`}>
              8GiB DDR4 3200MT/s 22CL Micron
            </div>
            <div class={`${transition} p-2 text-right`}>
              8GiB DDR4 3200MT/s 22CL Crucial
            </div>
          </TableCellHorizontalSplit>
        </TableCellVerticalSplit>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>Storage</div>
          <div class={`${transition} p-2 w-full text-right`}>
            512 GiB NVMe Western Digital SN530
          </div>
        </TableCellVerticalSplit>
      </Table>
      <Table>
        <div class="p-3 text-xl text-center">Software</div>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>OS</div>
          <div class={`${transition} p-2 w-full text-right`}>Arch Linux</div>
        </TableCellVerticalSplit>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>Text Editor</div>
          <div class={`${transition} p-2 w-full text-right`}>NeoVim</div>
        </TableCellVerticalSplit>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>VCS</div>
          <div class={`${transition} p-2 w-full text-right`}>Git</div>
        </TableCellVerticalSplit>
        <TableCellVerticalSplit>
          <div class={`${transition} p-2 w-1/3`}>Browser</div>
          <div class={`${transition} p-2 w-full text-right`}>Firefox</div>
        </TableCellVerticalSplit>
      </Table>
    </div>
  );
};

export default Setup;
