import { Component, For, JSXElement } from "solid-js";
import codeberg from "./assets/codeberg";
import discord from "./assets/discord";
import github from "./assets/github";
import gitlab from "./assets/gitlab";
import gmail from "./assets/gmail";
import instagram from "./assets/instagram";
import linkedin from "./assets/linkedin";
import tutanota from "./assets/tutanota";
import { transition } from "./utils";

const Home: Component = () => {
  return (
    <div class="flex flex-col space-y-6 text-gray-100/60">
      <div class="text-2xl">Hey, I'm</div>
      <div class="text-6xl font-bold text-gray-100/60">Dhruvin Purohit</div>
      <div class="text-2xl">
        I'm a {age()} year old software developer from India. I've been
        programming for {since()} years.
      </div>
      <div class="text-2xl">
        My interests are Desktop App Development, Web Development (Backend) and
        Server Side Applications.
      </div>
      <div class="grid grid-cols-4 justify-around gap-4">
        <For each={socials}>
          {(sc) => (
            <a
              class={`${transition} m-auto w-12 h-12 text-gray-100/30 hover:text-gray-100/60`}
              href={sc.link}
            >
              {sc.svg}
            </a>
          )}
        </For>
      </div>
    </div>
  );
};

const age = () =>
  Math.floor(
    (new Date().getTime() - new Date(2003, 11, 23).getTime()) / 31557600000,
  );

const since = () =>
  Math.floor(
    (new Date().getTime() - new Date(2020, 7, 7).getTime()) / 31557600000,
  );

const socials: ISocial[] = [
  {
    link: "https://codeberg.org/hellodhruvin",
    svg: codeberg,
  },
  {
    link: "https://linkedin.com/in/dhruvin-purohit",
    svg: linkedin,
  },
  {
    link: "mailto:dhruvin@tutanota.com",
    svg: tutanota,
  },
  {
    link: "mailto:hellodhruvin@gmail.com",
    svg: gmail,
  },
  {
    link: "https://dsc.gg/cafe",
    svg: discord,
  },
  {
    link: "https://gitlab.com/hellodhruvin",
    svg: gitlab,
  },
  {
    link: "https://github.com/hellodhruvin",
    svg: github,
  },
  {
    link: "https://www.instagram.com/dhruvin_purohit",
    svg: instagram,
  },
];

interface ISocial {
  link: string;
  svg: JSXElement;
}

export default Home;
