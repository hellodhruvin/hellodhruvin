import { Component, JSXElement } from "solid-js";
import { transition } from "../utils";

export const Table: Component<ITableProps> = (props) => {
  return (
    <div
      class={`${transition} group flex flex-col rounded-lg border-gray-100/30 border-2 divide-y-2 divide-gray-100/30 hover:(border-gray-100/60 divide-gray-100/60) ${props.class}`}
    >
      {props.children}
    </div>
  );
};

export const TableCellVerticalSplit: Component<ITableCellProps> = (props) => {
  return (
    <div
      class={`${transition} flex flex-row divide-x-2 divide-gray-100/30 group-hover:divide-gray-100/60 ${props.class}`}
    >
      {props.children}
    </div>
  );
};

export const TableCellHorizontalSplit: Component<ITableCellProps> = (props) => {
  return (
    <div
      class={`${transition} flex flex-col divide-y-2 divide-gray-100/30 group-hover:divide-gray-100/60 ${props.class}`}
    >
      {props.children}
    </div>
  );
};

interface ITableProps {
  children: JSXElement;
  class?: string;
}

interface ITableCellProps {
  children: JSXElement | string;
  class?: string;
}
