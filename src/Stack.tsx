import {
  Component,
  createSignal,
  For,
  JSXElement,
  Match,
  Show,
  Switch,
} from "solid-js";
import rust from "./assets/rust";
import typescript from "./assets/typescript";
import javascript from "./assets/javascript";
import python from "./assets/python";
import git from "./assets/git";
import docker from "./assets/docker";
import express from "./assets/express";
import fastify from "./assets/fastify";
import mongodb from "./assets/mongodb";
import nextjs from "./assets/nextjs";
import prisma from "./assets/prisma";
import react from "./assets/react";
import solidjs from "./assets/solidjs";
import sqlite from "./assets/sqlite";
import svelte from "./assets/svelte";
import tailwindcss from "./assets/tailwindcss";
import windicss from "./assets/windicss";
import { transition } from "./utils";

const Stack: Component = () => {
  const [selection, setSelection] = createSignal("");

  // TODO: Add staggering animation (delay over indexes, use safelist for maximum number of elements) to all divs
  return (
    <div class="flex flex-col w-full">
      <div class="flex flex-wrap justify-center w-full">
        <For each={languages}>
          {(lang) => (
            <div
              class={`${transition} m-2 h-18 w-18 rounded-lg cursor-pointer ${
                selection() === lang.name
                  ? `text-gray-100/80`
                  : `text-gray-100/30 hover:text-gray-100/60`
              }`}
              onClick={() => setSelection(lang.name)}
            >
              {lang.svg}
            </div>
          )}
        </For>
      </div>
      <div>
        <Switch
          fallback={
            <div
              class={`${transition} p-2 my-2 rounded-lg border-2 text-gray-100/30 border-gray-100/30 hover:border-gray-100/60`}
            >
              Select a language from above
            </div>
          }
        >
          <For each={languages}>
            {(lang) => (
              <Match when={selection() === lang.name}>
                <div
                  class={`${transition} p-2 my-2 rounded-lg border-2 text-gray-100/60 border-gray-100/30 hover:border-gray-100/60`}
                >
                  {lang.remarks}
                </div>
                <For each={lang.frameworks}>
                  {(fw) => (
                    <div
                      class={`${transition} group flex flex-row items-center my-2 w-full divide-x-2 divide-gray-100/30 rounded-lg border-2 border-gray-100/30 hover:(border-gray-100/60 divide-gray-100/60)`}
                    >
                      <div
                        class={`${transition} m-2 w-16 h-16 text-gray-100/30 group-hover:text-gray-100/60`}
                      >
                        <Show when={fw.svg} fallback={lang.svg}>
                          {fw.svg}
                        </Show>
                      </div>
                      <div class={`${transition} flex flex-col p-2 w-full`}>
                        <div class="py-1 text-xl text-gray-100/60">
                          {fw.name}
                        </div>
                        <div class="text-gray-100/60">{fw.remarks}</div>
                      </div>
                    </div>
                  )}
                </For>
              </Match>
            )}
          </For>
        </Switch>
      </div>
      <div class="flex flex-col w-full"></div>
    </div>
  );
};

export default Stack;

interface ILanguage {
  name: string;
  svg: JSXElement;
  remarks: string;
  frameworks: IFramework[];
}

interface IFramework {
  name: string;
  // TODO: Implement
  svg?: JSXElement;
  remarks: string;
}

const languages: ILanguage[] = [
  {
    name: "rust",
    svg: rust,
    remarks:
      "My favorite language. My go-to language for small applications. I am not an expert in this language.",
    frameworks: [
      {
        name: "warp",
        remarks:
          "Warp is my choice for small REST APIs because in a small project it's easier to maintain code written in warp. The filter approach of making REST APIs is just mind blowing. Not to mention the vert fast speed and functional programming approach.",
      },
      {
        name: "actix-web",
        remarks:
          "Actix-Web is my choice for huge and more complex REST APIs because it's much more easier to maintain huge codebase.",
      },
      {
        name: "poise",
        remarks:
          "Poise is my choice for creating performant discord bots in rust. If I were to make a discord bot framework in rust, It would have been like poise (or worse).",
      },
    ],
  },
  {
    name: "typescript",
    svg: typescript,
    remarks:
      "My second favorite language. My go-to language for web-applications or some server side application (using Node.JS). I am expert at this language.",
    frameworks: [
      {
        name: "prisma",
        remarks:
          "Prisma is my choice when it comes to ORM. It also supports so many databases and migration to another database is not much painful. Also reusable database models across multiple projects.",
        svg: prisma,
      },
      {
        name: "next.js",
        remarks:
          "Next.JS is my choice when it comes to a full stack React application.",
        svg: nextjs,
      },
      {
        name: "react",
        remarks:
          "I no longer use react for small projects. I exlusively use React with Next.JS only",
        svg: react,
      },
      {
        name: "svelte",
        remarks:
          "Svelte is my choice when it comes to making small projects, tied with Solid.JS.",
        svg: svelte,
      },
      {
        name: "solid.js",
        remarks:
          "Solid.JS is my choice when it comes to making small projects. No virtual DOM and really fast.",
        svg: solidjs,
      },

      {
        name: "fastify",
        remarks:
          "Fastify is my choice for making REST APIs if TypeScript/JavaScript is the only option. It's really much faster compared to express.",
        svg: fastify,
      },
      {
        name: "express",
        remarks:
          "Express is not my choice for making REST APIs. I just work on legacy code when using express and I encourage use of fastify for newer projects.",
        svg: express,
      },
      {
        name: "javascript",
        remarks:
          "My fallback language if typescript isn't an option or if the project is really small.",
        svg: javascript,
      },
    ],
  },
  {
    name: "other",
    svg: git,
    remarks: "Other things that I know but didn't want to categorize.",
    frameworks: [
      {
        name: "tailwindcss",
        remarks:
          "Tailwind is my choice for writing UIs in big projects. I don't like UI Libraries, they restrict a lot on freedom. I also don't like writing CSS by hand. Tailwind allows me to use css without even touching a css file. The naming of their classes are also short yet recognizable.",
        svg: tailwindcss,
      },
      {
        name: "windicss",
        remarks:
          "WindiCSS is my choice for writing UIs in small projects. WindiCSS is a viable tailwind alternative for small applications. It's also really easy to setup compared to tailwind and very fast becasue it's a compiler and not a PostCSS processor.",
        svg: windicss,
      },
      {
        name: "docker",
        remarks:
          "Pretty new to this, but this seems like an amazing tool. The container and Images approach also let's me test builds without buying an actual VPS.",
        svg: docker,
      },
      {
        name: "mongodb",
        remarks:
          "Most of the time my go-to database. NoSQL, JSON like format, and also one of the fastest databases.",
        svg: mongodb,
      },
      {
        name: "sql",
        remarks:
          "I use databases from this family when I have some complex relational data to store. SQLite, MySQl/MariaDB and PostgreSQL are the ones I use.",
        svg: sqlite,
      },
      {
        name: "python",
        svg: python,
        remarks:
          "My least favorite language from my stack. I don't use python much. I am expert at this language.",
      },
    ],
  },
];
