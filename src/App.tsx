import { Component, createSignal, For, Match, Switch } from "solid-js";
import Home from "./Home";
import Stack from "./Stack";
import Setup from "./Setup";
import Plans from "./Plans";
import More from "./More";
import { transition } from "./utils";

const App: Component = () => {
  const [route, setRoute] = createSignal("/");
  const navContents = ["/", "/stack", "/setup", "/plans", "/more"];
  return (
    <div class="flex flex-col justify-center items-center min-h-screen font-mono text-gray-100 min-w-screen bg-zinc-900">
      <div class="flex flex-col justify-start items-start min-h-screen w-prose">
        {/* ╭────────╮
            │ Navbar │
            ╰────────╯ */}
        <div class="flex justify-around items-start w-full font-semibold">
          <For each={navContents}>
            {(item) => (
              <div
                class={`${transition} flex justify-center items-center cursor-pointer py-3 px-6 font-bold flex-grow rounded-lg hover:bg-zinc-800 ${
                  route() === item
                    ? "text-gray-100/80"
                    : "text-gray-100/30 hover:text-gray-100/60"
                }`}
                onClick={() => setRoute(item)}
              >
                {item}
              </div>
            )}
          </For>
        </div>
        <div class="p-4 w-full">
          <Switch
            fallback={<div>Looks like you managed to break something ._.</div>}
          >
            <Match when={route() === "/"}>
              <Home />
            </Match>
            <Match when={route() === "/stack"}>
              <Stack />
            </Match>
            <Match when={route() === "/setup"}>
              <Setup />
            </Match>
            <Match when={route() === "/plans"}>
              <Plans />
            </Match>
            <Match when={route() === "/more"}>
              <More />
            </Match>
          </Switch>
        </div>
      </div>
    </div>
  );
};

export default App;
